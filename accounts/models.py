from django.db import models
from django.contrib.auth.models import AbstractUser



# def user_directory_path(instance, filename):
#     return "user_{0}/{1}".format(instance.user.id, filename)


class CustomUser(AbstractUser):
    date_of_birth = models.DateField()









class Board(models.Model):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


        
class Pin(models.Model):
    board = models.ForeignKey(Board, on_delete=models.CASCADE)
    title = models.CharField(max_length=255)
    catagory = models.CharField(max_length=255)  # Assuming you have a separate Tag model
    image = models.ImageField(upload_to='pins/')  # Image upload field
    description = models.CharField(max_length=255)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.title            

    
