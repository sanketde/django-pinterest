from django.urls import path
from . import views

urlpatterns = [
    path('', views.signup_view, name='signup'),
    path('success/',views.success,name='success'),
    path('login/', views.login_view, name='login'),
    path('dashboard/', views.dashboard_view, name='dashboard'),
    path('logout/', views.logout_view, name='logout'),
    path('board/', views.create_board, name='create_board'),
    path('create_pin/', views.create_pin, name='create_pin'),
    path('dashboard/pin/<int:id>/', views.pin_detail_view, name='pin'),
]
