const signupLink = document.getElementById('signupLink');
const signupCard = document.getElementById('signupCard');
const loginLink = document.getElementById('loginLink');
const loginCard = document.getElementById('loginCard');

signupLink.addEventListener("click", function (event) {
    event.preventDefault();
    signupCard.classList.remove("hidden");
    loginCard.classList.add("hidden");
});

document.getElementById("close-form-btn-signup").addEventListener("click", function (event) {
    event.preventDefault();
    document.getElementById("signupCard").classList.add("hidden");
});


loginLink.addEventListener("click", function (event) {
    event.preventDefault();
    document.getElementById("loginCard").classList.remove("hidden");
    signupCard.classList.add("hidden");
});

document.getElementById("close-form-btn-login").addEventListener("click", function (event) {
    event.preventDefault();
    document.getElementById("loginCard").classList.add("hidden");
});



// toogle password script
const togglePassword = document.querySelector('#togglePassword');
const password = document.querySelector('#id_password');

togglePassword.addEventListener('click', function (e) {
    // toggle the type attribute
    const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
    password.setAttribute('type', type);
    // toggle the eye slash icon
    this.classList.toggle('fa fa-eye-slash');
}); 

//  toogle password script
const togglePasswordLogin = document.querySelector('#togglePasswordLogin');
const passwordLogin = document.querySelector('#id_password_login');

togglePasswordLogin.addEventListener('click', function (e) {
    // toggle the type attribute
    const type = passwordLogin.getAttribute('type') === 'password' ? 'text' : 'password';
    passwordLogin.setAttribute('type', type);
    // toggle the eye slash icon
    this.classList.toggle('fa fa-eye-slash');
});


// error handling in the signup form

document.getElementById('signupForm').addEventListener('submit', function(event) {
    var username = document.getElementById('id_username').value;
    var password = document.getElementById('id_password').value;
    var dateOfBirth = document.getElementById('id_date_of_birth').value;

    var usernameError = document.getElementById('usernameError');
    var passwordError = document.getElementById('passwordError');
    var dateOfBirthError = document.getElementById('dateOfBirthError');
    
    usernameError.textContent = '';
    passwordError.textContent = '';
    dateOfBirthError.textContent = '';

    if (username.trim() === '') {
        event.preventDefault();  // Prevent form submission
        usernameError.textContent = 'Please enter your email address.';
    }
    if (password.trim() === '') {
        event.preventDefault();  // Prevent form submission
        passwordError.textContent = 'Please enter a password.';
    }
    if (dateOfBirth.trim() === '') {
        event.preventDefault();  // Prevent form submission
        dateOfBirthError.textContent = 'Please enter your date of birth.';
    }
});


