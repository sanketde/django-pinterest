
from django.shortcuts import render, redirect,get_object_or_404
from django.contrib.auth import login, authenticate,logout
from .forms import SignupForm
from .models import CustomUser
from .forms import BoardForm, PinForm
from .models import Board, Pin
from django.contrib.auth.decorators import login_required


def signup_view(request):
    if request.method == 'POST':
        form = SignupForm(request.POST, request.FILES)
        if form.is_valid():
            # Create a new user account
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            date_of_birth = form.cleaned_data['date_of_birth']

            # Create the user with an encrypted password
            user = CustomUser(username=username, date_of_birth=date_of_birth)
            user.set_password(password)  # Set the encrypted password
            user.save()
            
            # Authenticate the user and log them in
            user = authenticate(request, username=username, password=password)
            login(request, user)
            
            return redirect('dashboard')  
    else:
        form = SignupForm()
    
    return render(request, 'signup.html', {'form': form})

def success(request):
    return render(request, 'success.html')

@login_required
def dashboard_view(request):
    username = request.user.username  
    pins = Pin.objects.all()
    context = {'pins': pins ,'username': username}
    return render(request, 'dashboard.html', context)  


def login_view(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('dashboard')  
        else:
            error_message = 'Invalid username or password'  
            print(error_message)
            return render(request, 'signup.html', {'error_message': error_message})
    else:
        return render(request, 'login.html')


@login_required
def logout_view(request):
    logout(request)
    return redirect('signup')



# dashboard page views

@login_required
def create_board(request):
    if request.method == 'POST':
        board_form = BoardForm(request.POST)
        if board_form.is_valid():
            board = board_form.save(commit=False)
            board.user = request.user
            board.save()
            return redirect('dashboard')
    else:
        board_form = BoardForm()

    context = {
        'form': board_form,
    }
    return render(request, 'create_board.html', context)



# create pin view

@login_required
def create_pin(request):
    if request.method == 'POST':
        pin_form = PinForm(request.POST, request.FILES)
        if pin_form.is_valid():
            pin = pin_form.save(commit=False)
            print(request.POST)
            pin.board = get_board_object(request)  
            pin.save()
            return redirect('dashboard')
    else:
        pin_form = PinForm()

    context = {
        'pin_form': pin_form,
    }
    return render(request, 'create_pin.html', context)

def get_board_object(request):
    board_id = request.POST.get('board') 
    board = get_object_or_404(Board, id=board_id)
    return board

def pin_detail_view(request, id):
    pin_detail = Pin.objects.select_related('board__user').get(id=id)
    host = pin_detail.board.user.username

    context = {
        'pin_detail': pin_detail,
        'host' : host
    }
    return render(request, 'pin_detail.html', context)



